## Requirements

1. Linux or LSFW on Windows (No shell script support yet but it should be easy to port), if you want to run the makefile.
2. Package 'zip' (sudo apt install zip, sudo pacman -S zip, etc)
3. Package 'make' (Should be installed on any regular Linux distribution unless you are using a server edition/minimal edition. If not installed, install it).

## How to use

1. Modify this template accordingly and create your datapack using it.
2. Open 'makefile' and modify the following values:
    - zipname=\<name of your ZIP file\>
    - dptestdir=\<path to the datapack folder of your testing world (Used for the install rule, optional)\>
3. Open a shell into the root directory of the datapack, then execute 'make'.
4. (Optional) Execute 'make install' to copy the datapack into your Minecraft testing world. Then on Minecraft, with your testing world open, execute the command '/reload'.
5. Test your datapack.

# How to use it manually (on Windows, for now)

1. Select the 'data' folder, and the 'pack.mcmeta' and 'pack.png' files.
2. Right click on any of the selected files/folders, select 'Send to -> Compressed (zipped) folder'.
3. Specify a name for your ZIP file, this should be the name of your datapack. Follow the wizard to compress those files.
4. Once zipped, copy the .zip file into your world's datapacks folder (Usually %appdata%\\.minecraft\\saves\\\<world name\>\\datapacks)
5. Go back to Minecraft and execute the command '/reload'.
6. Test your datapack.

## How to create a datapack

There is a good starting guide at [minecraft.fandom.com](https://minecraft.fandom.com/wiki/Tutorials/Creating_a_data_pack). If you want more complex stuff, you can download other datapacks from the internet and use them as reference material.

Since datapacks are basically command scripts, you might want to check some information about [commands](https://minecraft.fandom.com/wiki/Commands) as well.

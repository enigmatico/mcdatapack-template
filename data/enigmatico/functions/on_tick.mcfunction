# Namespace: enigmatico
# Function: on_tick
# This function will run once per tick.

effect give @a[gamemode=!spectator] jump_boost 30 10 true
effect give @a[gamemode=!spectator] slow_falling 30 10 true

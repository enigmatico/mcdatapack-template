z=zip
zflags=-r
zipname=testpack-1.17.zip
dptestdir=~/.minecraft/saves/<world name>/datapacks/
all:
	$(z) $(zflags) $(zipname) ./data ./pack.png ./pack.mcmeta

install:
	cp $(zipname) $(dptestdir)
	@echo "Done. Use /reload in-game to reload the datapack"

clean:
	rm -r *.zip
